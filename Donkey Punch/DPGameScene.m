//
//  DPGameScene.m
//  Donkey Punch
//
//  Created by Ryan Salton on 12/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "DPGameScene.h"
#import "DPPlayer.h"

@implementation DPGameScene {
    DPPlayer *player;
}


-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    player = [DPPlayer spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(100, 200)];
    player.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.5);
    [self addChild:player];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
    
        
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
