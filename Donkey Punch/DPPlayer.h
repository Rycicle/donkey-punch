//
//  DPPlayer.h
//  Donkey Punch
//
//  Created by Ryan Salton on 12/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface DPPlayer : SKSpriteNode

- (void)checkLean;

@end
