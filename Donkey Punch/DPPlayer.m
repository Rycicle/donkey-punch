//
//  DPPlayer.m
//  Donkey Punch
//
//  Created by Ryan Salton on 12/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "DPPlayer.h"

@implementation DPPlayer

+ (instancetype)spriteNodeWithColor:(UIColor *)color size:(CGSize)size
{
    DPPlayer *player = [super spriteNodeWithColor:color size:size];
    
    player.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:30 center:CGPointMake(0, -player.size.height * 0.5)];
    player.physicsBody.dynamic = NO;
    
    return player;
}

- (void)checkLean
{
    
}

@end
